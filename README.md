# MultiCultiChat

A chat-engine that enables chat between partners in different languages.

Apache Kafka stores the chat messages
Spring Boot is used as in REST API between frontend and backend
ReactJS is used in Frontend
Google Translate is used for language detection and translation
LogStash push the Kafka topics to Elastic Search
Elastic Search stores log data
Kibana displays logs graphically