package io.multicultichat.kafkaProducer.resource.service;

import io.multicultichat.kafkaProducer.model.ChatData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class ChatService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.topic.name}")
    private String topicName;

    public void sendChatData(ChatData chatData) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(KafkaHeaders.TOPIC, topicName);
        kafkaTemplate.send(new GenericMessage<ChatData>(chatData, headers));
//       Use below to send String through Kafka
//        kafkaTemplate.send(topicName, "VALUE TO SEND");
        LOGGER.info("DATA => " + chatData.toString() + "<= sent to KAFKA TOPIC = " + topicName);
    }

}
