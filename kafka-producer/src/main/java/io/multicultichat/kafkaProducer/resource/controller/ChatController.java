package io.multicultichat.kafkaProducer.resource.controller;

import io.multicultichat.kafkaProducer.model.ChatData;
import io.multicultichat.kafkaProducer.resource.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatService chatService;

    @PostMapping
    public ResponseEntity<String> sendChatData(@RequestBody ChatData chatData) {
        chatService.sendChatData(chatData);
        return new ResponseEntity<>("Multi Culti chat data send to KAFKA", HttpStatus.OK);
    }
}
