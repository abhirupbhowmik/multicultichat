package io.multicultichat.kafkaProducer.model;

public class ChatData {

    private String userName;
    private String userEmailId;
    private String userMessage;
    private String userLanguage;

    public ChatData() {
    }

    public ChatData(String userName, String userEmailId, String userMessage, String userLanguage) {
        this.userName = userName;
        this.userEmailId = userEmailId;
        this.userMessage = userMessage;
        this.userLanguage = userLanguage;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmailId() {
        return userEmailId;
    }

    public void setUserEmailId(String userEmailId) {
        this.userEmailId = userEmailId;
    }

    public String getUserMessage() {
        return userMessage;
    }

    public void setUserMessage(String userMessage) {
        this.userMessage = userMessage;
    }

    public String getUserLanguage() {
        return userLanguage;
    }

    public void setUserLanguage(String userLanguage) {
        this.userLanguage = userLanguage;
    }

    @Override
    public String toString() {
        return "ChatData{" +
                "userName='" + userName + '\'' +
                ", userEmailId='" + userEmailId + '\'' +
                ", userMessage='" + userMessage + '\'' +
                ", userLanguage='" + userLanguage + '\'' +
                '}';
    }
}