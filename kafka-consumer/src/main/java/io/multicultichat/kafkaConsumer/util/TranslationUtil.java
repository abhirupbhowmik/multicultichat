package io.multicultichat.kafkaConsumer.util;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import io.multicultichat.kafkaConsumer.model.ChatData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TranslationUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TranslationUtil.class);

    public static String translateChatContent(ChatData chatData) {
        // Instantiates a Google client using Environment Variable GOOGLE_APPLICATION_CREDENTIALS that points to a json-file containing

        Translate translate = TranslateOptions.getDefaultInstance().getService();
        // Detect the language of the Chat Message sent by User
        String detectedChatLanguage = "", translatedMessage = "";
        if (chatData != null && !chatData.getUserMessage().isBlank()) {
            detectedChatLanguage = translate.detect(chatData.getUserMessage()).getLanguage();
            if (detectedChatLanguage != null && !detectedChatLanguage.isBlank()) {
                translatedMessage = translate.translate(chatData.getUserMessage(), Translate.TranslateOption.sourceLanguage(detectedChatLanguage), Translate.TranslateOption.targetLanguage(chatData.getUserLanguage())).getTranslatedText();
            }
        }
        LOGGER.info("ORIGINAL MESSAGE IN " + detectedChatLanguage.toUpperCase() + " => " + chatData.getUserMessage());
        return translatedMessage;
    }
}
