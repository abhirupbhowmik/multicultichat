package io.multicultichat.kafkaConsumer.resource;

import io.multicultichat.kafkaConsumer.model.ChatData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ChatService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Value("${kafka.topic.translated.name}")
    private String translatedTopicName;

    @Value("${elastic.search.url}")
    private String elasticSearchUrl;

    public void sendTranslatedChatData(ChatData translatedChatData) {
        Map<String, Object> headers = new HashMap<>();
        headers.put(KafkaHeaders.TOPIC, translatedTopicName);
//        Sending the translated chat data to KAFKA
        kafkaTemplate.send(new GenericMessage<ChatData>(translatedChatData, headers));
        LOGGER.info("DATA => " + translatedChatData.toString() + "<= sent to KAFKA TOPIC = " + translatedTopicName);

        sendTranslatedChatDataToElasticSearch(translatedChatData);
    }

    public void sendTranslatedChatDataToElasticSearch(ChatData translatedChatData) {
        RestTemplate restTemplate = new RestTemplate();
        elasticSearchUrl = elasticSearchUrl + "/" + translatedTopicName;
        try {
            URI elasticUri = new URI(elasticSearchUrl);
            ResponseEntity<ChatData> result = restTemplate.postForEntity(elasticUri, translatedChatData, ChatData.class);
            LOGGER.info("Translated Chat Data send to Elastic Search with staus code : " + result.getStatusCodeValue());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
