package io.multicultichat.kafkaConsumer.listener;

import io.multicultichat.kafkaConsumer.model.ChatData;
import io.multicultichat.kafkaConsumer.resource.ChatService;
import io.multicultichat.kafkaConsumer.util.TranslationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaConsumer.class);

    @Autowired
    private ChatService chatService;

    @KafkaListener(topics = "${kafka.topic.name}", groupId = "${kafka.consumer.group.id}")
    public void consumeChatDataAndSendTranslatedMessage(ChatData chatData) {
        LOGGER.info("DATA => " + chatData.toString() + "<= received ");
        LOGGER.info("ORIGINAL MESSAGE IN => " + chatData.getUserMessage());
        chatData.setUserMessage(TranslationUtil.translateChatContent(chatData));
        chatService.sendTranslatedChatData(chatData);
    }

    @KafkaListener(topics = "${kafka.topic.translated.name}", groupId = "${kafka.consumer.group.id}")
    public void consumeTranslatedChatData(ChatData translatedChatData) {
        LOGGER.info("================================================================================================");
        LOGGER.info("TRANSLATED MESSAGE IN " + translatedChatData.getUserLanguage().toUpperCase() + " => " + translatedChatData.getUserMessage());
    }
}
