package io.multicultichat.kafkaConsumer.config;

import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KakfaProducerConfiguration {

    @Value("${kafka.boot.server}")
    private String kafkaServer;

    /*This Template is used for Spring Auto Configuration of Kafka. In Production you should set them accordingly */
    @Bean
    public KafkaTemplate<String, String> kafkaTemplate() {
        return new KafkaTemplate<String, String>(producerFactory());
    }

    @Bean
    public ProducerFactory<String, String> producerFactory() {
        Map<String, Object> producerFactoryConfig = new HashMap<>();

        producerFactoryConfig.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        producerFactoryConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
        producerFactoryConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        producerFactoryConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
//      Uncomment the below lines if you want to send String instead of an Object through Kafka
//        producerFactoryConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        return new DefaultKafkaProducerFactory<>(producerFactoryConfig);
    }

}
